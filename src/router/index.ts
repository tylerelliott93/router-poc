import { createRouter, createWebHistory } from 'vue-router';
import Map from '../components/Map.vue';
import Tasks from '../components/Tasks.vue';
import TaskDetail from '../components/TaskDetail.vue';
import Context from '../components/Context.vue';
import Workspace from '../components/Workspace.vue';

const routes = [
  {
    path: '/',
    component: Workspace,
    children: [
      {
        path: '',
        components: {
          default: Map,
          taskList: Tasks,
          taskDetail: TaskDetail,
          context: Context
        }
      },
      {
        path: 'task/:id',
        name: 'task',
        components: {
          default: Map,
          taskList: Tasks,
          taskDetail: TaskDetail,
          context: Context
        },
        props: true,
        children: [
          {
            path: 'context/:contextId',
            name: 'context',
            components: {
              default: Map,
              taskList: Tasks,
              taskDetail: TaskDetail,
              context: Context
            },
            props: true
          }
        ]
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
